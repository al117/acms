-- Air Companies
INSERT INTO air_company (air_company_name, company_type, founded_at) VALUES ('LvivAir', 'Flying','2003-03-04 16:28:35');
INSERT INTO air_company (air_company_name, company_type, founded_at) VALUES ('TestAirlines', 'Flying','1998-11-15 16:28:35');
INSERT INTO air_company (air_company_name, company_type, founded_at) VALUES ('FlyWay', 'Flying','2010-11-11 16:28:35');

-- airplanes from company 1.
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air1_1','air1type','2020-10-04 16:28:35','fsn12384575',3000,1000,20,1);
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air1_2','air1type','2020-08-11 16:28:35','fsn93742312',3500,1200,17,1);
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air1_3','air1type','2020-06-21 16:28:35','fsn54623341',3200,1100,11,1);

-- airplanes from company 2.
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air2_1','air2type','2020-10-25 16:28:35','fsn09324712',3000,1000,8,2);
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air2_2','air2type','2020-09-14 16:28:35','fsn00324423',3500,1200,3,2);
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air2_3','air2type','2020-02-19 16:28:35','fsn05465342',4000,1500,6,2);

-- airplanes from company 3.
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air3_1','air3type','2020-01-14 16:28:35','fsn12384575',2700,800,24,3);
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air3_2','air3type','2020-02-15 16:28:35','fsn93742312',2800,850,25,3);
INSERT INTO airplane (airplane_name, airplane_type, created_at, factory_serial_number, flight_distance, fuel_capacity, number_of_flights, air_company_id) VALUES ('air3_3','air3type','2020-03-16 16:28:35','fsn54623341',2900,900,26,3);

-- FLIGHTS

-- completed in time
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2020-01-14 16:28:35', null,'Ukraine', 'France', 1200, '2020-01-14 19:28:34', 7200000, 1, '2020-01-14 17:28:35', 1,1);
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2020-02-14 16:28:35', null,'Ukraine', 'German', 1300, '2020-02-14 22:24:34', 7200000, 1, '2020-02-14 20:28:35', 2,4);
-- Active more than 24h
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2021-02-02 12:28:35', null,'France', 'Ukraine', 1200, null, 7200000, 0, '2021-02-02 13:28:35', 1,2);
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2021-02-02 12:10:35', null,'German', 'Ukraine', 1300, null, 7200000, 0, '2021-02-02 13:10:35', 2,5);
-- completed late
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2020-03-10 11:28:35', null,'Japan', 'German', 9400, '2020-03-12 23:24:34', 64000000, 1, '2020-03-10 12:28:35', 3,8);
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2020-04-11 13:28:35', null,'Italy', 'France', 500, '2020-04-13 23:24:34', 5400000, 1, '2020-04-11 14:28:35', 3,9);
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2020-05-12 12:28:35', null,'USA', 'Poland', 7000, '2020-05-14 18:24:34', 48000000, 1, '2020-05-12 13:28:35', 3,7);
INSERT INTO flight (created_at, delay_started_at, departure_country, destination_country, distance, ended_at, estimated_flight_time, flight_status, started_at, air_company_id, airplane_id) VALUES ('2020-06-13 14:28:35', null,'Russia', 'China', 5800, '2020-06-15 23:24:34', 36000000, 1, '2020-06-13 15:28:35', 3,8);