package com.bohdan.kravets.acms.dto.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FlightRequest {
    private String departureCountry;

    private String destinationCountry;

    private int distance;

    private Long estimatedFlightTime;

    private Long airCompanyId;

    private Long airPlaneId;
}
