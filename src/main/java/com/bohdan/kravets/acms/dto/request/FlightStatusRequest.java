package com.bohdan.kravets.acms.dto.request;

import com.bohdan.kravets.acms.entity.FlightStatus;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FlightStatusRequest {

    private FlightStatus flightStatus;

    private Long flightId;
}
