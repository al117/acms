package com.bohdan.kravets.acms.dto.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AirPlaneRequest {

    private String airplaneName;

    private String factorySerialNumber;

    private int numberOfFlights;

    private int flightDistance;

    private int fuelCapacity;

    private String airplaneType;

    private Long airCompanyId;
}
