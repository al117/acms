package com.bohdan.kravets.acms.dto.request;

import com.bohdan.kravets.acms.entity.FlightStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightByStatusRequest {

    private FlightStatus flightStatus;

    private String airCompanyName;
}
