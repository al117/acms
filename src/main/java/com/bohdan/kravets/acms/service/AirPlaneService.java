package com.bohdan.kravets.acms.service;

import com.bohdan.kravets.acms.dto.request.AirPlaneRequest;
import com.bohdan.kravets.acms.entity.AirCompany;
import com.bohdan.kravets.acms.entity.Airplane;
import com.bohdan.kravets.acms.repository.AirCompanyRepository;
import com.bohdan.kravets.acms.repository.AirplaneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class AirPlaneService {

    @Autowired
    private AirplaneRepository airplaneRepository;

    @Autowired
    private AirCompanyRepository airCompanyRepository;


    public void movePlaneToAnotherCompany(Long planeId, Long companyId){
        Airplane airplaneFromDB = airplaneRepository.findAirplaneById(planeId);
        if (airplaneFromDB == null){
            throw new IllegalArgumentException("Airplane doesnt exist");
        } else {
            AirCompany airCompanyFromDB = airCompanyRepository.findAirCompaniesById(companyId);
            if (airCompanyFromDB == null){
                throw new IllegalArgumentException("Air company doesnt exist");
            } else {
                airplaneFromDB.setAirCompany(airCompanyFromDB);
                airplaneRepository.save(airplaneFromDB);
            }
        }
    }

    public void addAirPlane(AirPlaneRequest airPlaneRequest){
        Airplane airplane = new Airplane();
        airplane.setAirplaneName(airPlaneRequest.getAirplaneName());
        airplane.setAirplaneType(airPlaneRequest.getAirplaneType());
        airplane.setCreatedAt(Calendar.getInstance());
        airplane.setFactorySerialNumber(airPlaneRequest.getFactorySerialNumber());
        airplane.setFlightDistance(airPlaneRequest.getFlightDistance());
        airplane.setFuelCapacity(airPlaneRequest.getFuelCapacity());
        airplane.setNumberOfFlights(airPlaneRequest.getNumberOfFlights());
        if (airPlaneRequest.getAirCompanyId() == null){

        } else {
            airplane.setAirCompany(airCompanyRepository.findAirCompaniesById(airPlaneRequest.getAirCompanyId()));
        }
        airplaneRepository.save(airplane);
    }

    public List<Airplane> findAllAirPlanes(){
        List<Airplane> airplanes = airplaneRepository.findAll();
        return airplanes;
    }

}
