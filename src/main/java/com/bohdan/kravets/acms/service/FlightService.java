package com.bohdan.kravets.acms.service;

import com.bohdan.kravets.acms.dto.request.FlightByStatusRequest;
import com.bohdan.kravets.acms.dto.request.FlightRequest;
import com.bohdan.kravets.acms.dto.request.FlightStatusRequest;
import com.bohdan.kravets.acms.entity.AirCompany;
import com.bohdan.kravets.acms.entity.Flight;
import com.bohdan.kravets.acms.entity.FlightStatus;
import com.bohdan.kravets.acms.repository.AirCompanyRepository;
import com.bohdan.kravets.acms.repository.AirplaneRepository;
import com.bohdan.kravets.acms.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private AirCompanyRepository airCompanyRepository;

    @Autowired
    private AirplaneRepository airplaneRepository;


    public List<Flight> findCompanyFlightsByStatus(FlightByStatusRequest flightByStatusRequest){
        List<Flight> flights;
        AirCompany airCompanyFromDB = airCompanyRepository.findAirCompaniesByAirCompanyName(flightByStatusRequest.getAirCompanyName());
        if (airCompanyFromDB == null){
            throw new IllegalArgumentException("Air company doesnt exist");
        } else {
            flights = flightRepository.findAllByFlightStatusAndAirCompany_AirCompanyName(flightByStatusRequest.getFlightStatus(), flightByStatusRequest.getAirCompanyName());
        }
        return flights;
    }

    //*** FIND ALL FLIGHTS STARTED MORE THAN 24 HOURS AGO; STATUS = ACTIVE. 86400000ms = 24h***
    public List<Flight> findActiveFlights(){
        List<Flight> flightsActive = flightRepository.findAllByFlightStatus(FlightStatus.ACTIVE);
        return flightsActive.stream()
                .filter(flight -> (Calendar.getInstance().getTimeInMillis() - flight.getCreatedAt().getTimeInMillis()) > 86400000 )
                .collect(Collectors.toList());
    }

    public void createFlight(FlightRequest flightRequest){
        Flight flight = new Flight();
        flight.setCreatedAt(Calendar.getInstance());
        flight.setDepartureCountry(flightRequest.getDepartureCountry());
        flight.setDestinationCountry(flightRequest.getDestinationCountry());
        flight.setDistance(flightRequest.getDistance());
        flight.setEstimatedFlightTime(flightRequest.getEstimatedFlightTime());
        flight.setAirCompany(airCompanyRepository.findAirCompaniesById(flightRequest.getAirCompanyId()));
        flight.setAirplane(airplaneRepository.findAirplaneById(flightRequest.getAirPlaneId()));
        flight.setFlightStatus(FlightStatus.PENDING);

        flightRepository.save(flight);
    }

    public void changeFlightStatus(FlightStatusRequest flightStatusRequest){
        Flight flight = flightRepository.findFlightById(flightStatusRequest.getFlightId());
        if (flight == null){
            throw new IllegalArgumentException("Flight doesnt exist");
        } else {
            FlightStatus flightStatus = flightStatusRequest.getFlightStatus();

            if (flightStatus == FlightStatus.DELAYED){
                flight.setDelayStartedAt(Calendar.getInstance());
            } else if (flightStatus == FlightStatus.ACTIVE){
                flight.setStartedAt(Calendar.getInstance());
            } else if(flightStatus == FlightStatus.COMPLETED){
                flight.setEndedAt(Calendar.getInstance());
            }
            flight.setFlightStatus(flightStatus);
        }

        flightRepository.save(flight);
    }

    public List<Flight> findAllCompletedLate(){
        List<Flight> flights = flightRepository.findAllByFlightStatus(FlightStatus.COMPLETED);
        List<Flight> flightsLate = flights.stream().
                filter(flight -> (flight.getEndedAt().getTimeInMillis() - flight.getStartedAt().getTimeInMillis()) > flight.getEstimatedFlightTime())
                .collect(Collectors.toList());
        return flightsLate;
    }

}
