package com.bohdan.kravets.acms.service;

import com.bohdan.kravets.acms.entity.AirCompany;
import com.bohdan.kravets.acms.repository.AirCompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirCompanyService {

    @Autowired
    private AirCompanyRepository airCompanyRepository;

// *** Simple CRUD start ***
    public void createAirCompany(AirCompany airCompany){
        AirCompany airCompanyFromDB = airCompanyRepository.findAirCompaniesByAirCompanyName(airCompany.getAirCompanyName());
        if (airCompanyFromDB == null){
            airCompanyRepository.save(airCompany);
        } else {
            throw new IllegalArgumentException("Air company "+ airCompany.getAirCompanyName()+ " already exist");
        }
    }

    public List<AirCompany> findAllAirCompanies(){
        List<AirCompany> airCompanies = airCompanyRepository.findAll();
        return airCompanies;
    }

    public void updateAirCompanyById(AirCompany airCompany, Long airCompanyId){
        AirCompany airCompanyFromDB = airCompanyRepository.findAirCompaniesById(airCompanyId);
        if (airCompanyFromDB == null){
            throw new IllegalArgumentException("Air company doesnt exist");
        } else {
            airCompanyFromDB.setAirCompanyName(airCompany.getAirCompanyName());
            airCompanyFromDB.setCompanyType(airCompany.getCompanyType());
            airCompanyFromDB.setFoundedAt(airCompany.getFoundedAt());
            airCompanyRepository.save(airCompanyFromDB);
        }
    }

    public void deleteAirCompanyById(Long airCompanyId){
        AirCompany airCompanyFromDB = airCompanyRepository.findAirCompaniesById(airCompanyId);
        if (airCompanyFromDB == null){
            throw new IllegalArgumentException("Air company doesnt exist");
        } else {
            airCompanyRepository.deleteById(airCompanyId);
        }
    }
// *** Simple CRUD end ***



}
