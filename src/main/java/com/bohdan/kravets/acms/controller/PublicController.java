package com.bohdan.kravets.acms.controller;

import com.bohdan.kravets.acms.dto.request.AirPlaneRequest;
import com.bohdan.kravets.acms.dto.request.FlightByStatusRequest;
import com.bohdan.kravets.acms.dto.request.FlightRequest;
import com.bohdan.kravets.acms.dto.request.FlightStatusRequest;
import com.bohdan.kravets.acms.entity.AirCompany;
import com.bohdan.kravets.acms.entity.Airplane;
import com.bohdan.kravets.acms.entity.Flight;
import com.bohdan.kravets.acms.service.AirCompanyService;
import com.bohdan.kravets.acms.service.AirPlaneService;
import com.bohdan.kravets.acms.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/public")
public class PublicController {

    @Autowired
    private AirPlaneService airPlaneService;

    @Autowired
    private AirCompanyService airCompanyService;

    @Autowired
    private FlightService flightService;


    @PostMapping("/create/company")
    public void createAirCompany(@RequestBody AirCompany airCompany){
        airCompanyService.createAirCompany(airCompany);
    }

    @GetMapping("/companies")
    public List<AirCompany> findAllAirCompanies(){
        return airCompanyService.findAllAirCompanies();
    }

    @PutMapping("/update/company/by/{airCompanyId}")
    public void updateAirCompanyById(@RequestBody AirCompany airCompany, @PathVariable Long airCompanyId){
        airCompanyService.updateAirCompanyById(airCompany,airCompanyId);
    }

    @DeleteMapping("/delete/company/by/{airCompanyId}")
    public void deleteAirCompanyById(@PathVariable Long airCompanyId){
        airCompanyService.deleteAirCompanyById(airCompanyId);
    }

    @PostMapping("/create/plane")
    public void addAirPlane(@RequestBody AirPlaneRequest airPlaneRequest){
        airPlaneService.addAirPlane(airPlaneRequest);
    }

    @GetMapping("/planes")
    public List<Airplane> findAllAirPlanes(){
        return airPlaneService.findAllAirPlanes();
    }

    @PutMapping("/update/plane/by/{planeId}/and/{companyId}")
    public void movePlaneToAnotherCompany(@PathVariable Long planeId, @PathVariable Long companyId){
        airPlaneService.movePlaneToAnotherCompany(planeId, companyId);
    }



    @PostMapping("/create/flight")
    public void createFlight(@RequestBody FlightRequest flightRequest){
        flightService.createFlight(flightRequest);
    }

    @GetMapping("/flight/active/24h")
    public List<Flight> findActiveFlights(){
        return flightService.findActiveFlights();
    }

    @PutMapping("/flight/change/status")
    public void changeFlightStatus(@RequestBody FlightStatusRequest flightStatusRequest){
        flightService.changeFlightStatus(flightStatusRequest);
    }

    @GetMapping("/flight/completed/late")
    public List<Flight> findAllCompletedLate(){
        return flightService.findAllCompletedLate();
    }

    @GetMapping("/flight/by/status")
    public List<Flight> findCompanyFlightsByStatus(@RequestBody FlightByStatusRequest flightByStatusRequest){
        return flightService.findCompanyFlightsByStatus(flightByStatusRequest);
    }
}
