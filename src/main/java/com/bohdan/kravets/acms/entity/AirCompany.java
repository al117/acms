package com.bohdan.kravets.acms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@ToString
public class AirCompany {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String airCompanyName;

    private String companyType;

    private Calendar foundedAt;

    @JsonIgnore
    @OneToMany(mappedBy = "airCompany")
    private List<Airplane> airplanes = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "airCompany")
    private List<Flight> flights = new ArrayList<>();

}
