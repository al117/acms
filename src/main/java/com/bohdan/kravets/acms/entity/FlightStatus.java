package com.bohdan.kravets.acms.entity;

public enum FlightStatus {
    ACTIVE, COMPLETED, DELAYED, PENDING;
}
