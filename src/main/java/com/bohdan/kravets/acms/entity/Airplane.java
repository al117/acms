package com.bohdan.kravets.acms.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Airplane {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String airplaneName;

    private String factorySerialNumber;

    private int numberOfFlights;

    private int flightDistance;

    private int fuelCapacity;

    private String airplaneType;

    private Calendar createdAt;

    @ManyToOne
    private AirCompany airCompany;

    @JsonIgnore
    @OneToMany(mappedBy = "airplane")
    private List<Flight> flights = new ArrayList<>();

}
