package com.bohdan.kravets.acms.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private FlightStatus flightStatus;

    private String departureCountry;

    private String destinationCountry;

    private int distance;

    private Long estimatedFlightTime;

    private Calendar endedAt;

    private Calendar delayStartedAt;

    private Calendar createdAt;

    private Calendar startedAt;

    @ManyToOne
    private AirCompany airCompany;

    @ManyToOne
    private Airplane airplane;

}
