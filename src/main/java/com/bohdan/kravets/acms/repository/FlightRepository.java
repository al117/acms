package com.bohdan.kravets.acms.repository;

import com.bohdan.kravets.acms.entity.Flight;
import com.bohdan.kravets.acms.entity.FlightStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight,Long> {
    Flight findFlightById(Long flightId);
    List<Flight> findAllByFlightStatusAndAirCompany_AirCompanyName(FlightStatus flightStatus, String airCompanyName);
    List<Flight> findAllByFlightStatus(FlightStatus flightStatus);
}
