package com.bohdan.kravets.acms.repository;

import com.bohdan.kravets.acms.entity.AirCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirCompanyRepository extends JpaRepository<AirCompany,Long> {
    AirCompany findAirCompaniesByAirCompanyName(String airCompanyName);
    AirCompany findAirCompaniesById(Long airCompanyId);
}
