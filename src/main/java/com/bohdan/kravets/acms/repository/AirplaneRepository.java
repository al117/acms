package com.bohdan.kravets.acms.repository;

import com.bohdan.kravets.acms.entity.Airplane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirplaneRepository extends JpaRepository<Airplane,Long> {
    Airplane findAirplaneById(Long airplaneId);
}
