FROM openjdk:8
ADD target/docker-acms.jar docker-acms.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "docker-acms.jar"]